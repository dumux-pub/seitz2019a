#!/bin/sh

### Create a folder for the dune and dumux modules
### Go into the folder and execute this script

if [ -d dune-common ]; then
  echo "error: A directory named dune-common already exists."
  echo "Aborting."
  exit 1
fi

### Clone the necessary modules
git clone https://gitlab.dune-project.org/core/dune-common.git
git clone https://gitlab.dune-project.org/core/dune-geometry.git
git clone https://gitlab.dune-project.org/core/dune-grid.git
git clone https://gitlab.dune-project.org/core/dune-istl.git
git clone https://gitlab.dune-project.org/core/dune-localfunctions.git
git clone https://gitlab.dune-project.org/extensions/dune-alugrid
git clone https://gitlab.dune-project.org/extensions/dune-foamgrid.git 
git clone http://numerik.mi.fu-berlin.de/dune-subgrid/index.php
git clone https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git dumux
git clone https://git.iws.uni-stuttgart.de/dumux-pub/seitz2019a.git


### Go to specific branches
cd dune-alugrid && git checkout releases/2.6 && cd ..
cd dune-common && git checkout releases/2.6 && cd ..
cd dune-geometry && git checkout releases/2.6 && cd ..
cd dune-grid && git checkout releases/2.6 && cd ..
cd dune-istl && git checkout releases/2.6 && cd ..
cd dune-localfunctions && git checkout releases/2.6 && cd ..
cd dune-foamgrid && git checkout releases/2.6 && cd ..
cd dune-subgrid && git checkout releases/2.6-1 && cd ..

### Go to specific commits
cd dumux && git checkout bde9f86af79ea1f35a9493743985b0cad0b3f13a && cd ..

### Run dunecontrol
./dune-common/bin/dunecontrol --opts=seitz2019a/cmake.opts all

