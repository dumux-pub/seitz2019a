// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup OnePNCMinTests
 * \brief Definition of the spatial parameters for the thermochemistry
 *        problem which uses the non-insothermal 1pncmin model
 */

#ifndef DUMUX_THERMOCHEM_SPATIAL_PARAMS_HH
#define DUMUX_THERMOCHEM_SPATIAL_PARAMS_HH

#include <dumux/material/spatialparams/fv1p.hh>

#include <dumux/material/fluidmatrixinteractions/permeabilitykozenycarman.hh>
#include "permeabilitypowerlaw.hh"
#include <dumux/common/exceptions.hh>

namespace Dumux
{
/*!
 * \ingroup TwoPTwoCModel
 * \ingroup BoxTestProblems
 * \brief Definition of the spatial parameters for the FuelCell
 *        problem which uses the isothermal 2p2c box model
 */

template<class FVGridGeometry, class Scalar>
class ThermoChemSpatialParams
: public FVSpatialParamsOneP<FVGridGeometry, Scalar,
                             ThermoChemSpatialParams<FVGridGeometry, Scalar>>
{
    using GridView = typename FVGridGeometry::GridView;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using Element = typename GridView::template Codim<0>::Entity;
    using ParentType = FVSpatialParamsOneP<FVGridGeometry, Scalar,
                                           ThermoChemSpatialParams<FVGridGeometry, Scalar>>;

    enum { dimWorld=GridView::dimensionworld };

    using GlobalPosition = Dune::FieldVector<Scalar, dimWorld>;

public:
    // type used for the permeability (i.e. tensor or scalar)
    using PermeabilityType = Scalar;
    /*!
     * \brief The constructor
     *
     * \param fvGridGeometry The finite volume grid geometry
     */
    ThermoChemSpatialParams(std::shared_ptr<const FVGridGeometry> fvGridGeometry)
    : ParentType(fvGridGeometry)
    {
      // get whether porosity change is regarded (0) or not (1)
      isPoroChange_ = getParam<bool>("Problem.IsPoroChange");
      // get whether permeability change is regarded (0) or not (1)
      isPermChange_ = getParam<bool>("Problem.IsPermChange");
      // get which type of porosity-permeability-relation is used: kozeny-carman (1), or powerlaw (0)
      permeabilityLaw_ = getParam<bool>("Problem.PermeabilityLaw");
    }

    /*! Intrinsic permeability tensor K \f$[m^2]\f$ depending
     *  on the position in the domain
     *
     *  \param element The finite volume element
     *  \param scv The sub-control volume
     *  \param elemSol The element solution
     *
     *  Solution dependent permeability function
     */
    template<class ElementSolution>
    PermeabilityType permeability(const Element& element,
                                  const SubControlVolume& scv,
                                  const ElementSolution& elemSol) const
    {
        // constant permeability
        if(isPermChange_ ==1) return 5.221e-13;

        // changing permeability
        if(isPermChange_ == 0){

            auto priVars = evalSolution(element, element.geometry(), elemSol, scv.center());

            Scalar sumPrecipitates = 0.0;
            for (unsigned int solidPhaseIdx = 0; solidPhaseIdx < 2; ++solidPhaseIdx)
            sumPrecipitates += priVars[/*numComp*/2 + solidPhaseIdx];

            using std::max;
            const auto poro = 1 - sumPrecipitates;

            //power law
            if(permeabilityLaw_ == 0){
                Scalar referencePermeability = 5.22e-11; //chosen such, that min permeability is 5.221e-13;

                return permLawPower_.evaluatePermeability(referencePermeability, 0.8, poro);
            }

            //Kozeny-Carman law
            if(permeabilityLaw_ == 1){ 

                Scalar referencePermeability = 5.0e-12; //chosen such, that min permeability is 5.221e-13;

                return permLawKozeny_.evaluatePermeability(referencePermeability, 0.8, poro);
            }
            else DUNE_THROW(Dune::NotImplemented, "Only Kozeny-Carman and PowerLaw implemented");
        }
        else DUNE_THROW(Dune::InvalidStateException, "Invalid  Value of isPermChange_ " << isPermChange_);
    }

    /*!
     *  \brief Define the minimum porosity \f$[-]\f$ after clogging caused by mineralization
     *
     *  \param element The finite element
     *  \param scv The sub-control volume
     *  \param elemSol The element solution
     */
    template<class ElementSolution>
    Scalar porosity(const Element& element,
                    const SubControlVolume& scv,
                    const ElementSolution& elemSol) const
    {
        if(isPoroChange_ == 1) return 0.599;

        auto priVars = evalSolution(element, element.geometry(), elemSol, scv.center());

        Scalar sumPrecipitates = 0.0;
        for (unsigned int solidPhaseIdx = 0; solidPhaseIdx < 2; ++solidPhaseIdx)
            sumPrecipitates += priVars[/*numComp*/2  + solidPhaseIdx];

        return 1- sumPrecipitates;
    }

private:
   bool isExtension_(const GlobalPosition &globalPos) const
   { return globalPos[0] > 0.08 - eps_; }

   bool isExtension2_(const GlobalPosition &globalPos) const
   { return globalPos[0] > 0.085 - eps_; }

   Scalar eps_;

   bool isPoroChange_;
   bool isPermChange_;
   bool permeabilityLaw_;

   PermeabilityKozenyCarman<PermeabilityType> permLawKozeny_;
   PermeabilityPowerLaw<PermeabilityType> permLawPower_;


};

}//end namespace

#endif
