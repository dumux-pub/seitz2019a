// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup OnePNCMinTests
 * \brief Definition of a problem for thermochemical heat storage using \f$ \textnormal{CaO},   \textnormal{Ca} \left( \textnormal{OH} \right)_2\f$.
 */
#ifndef DUMUX_CHARGE_PROBLEM_HH
#define DUMUX_CHARGE_PROBLEM_HH

#include <dumux/porousmediumflow/1pncmin/model.hh>
#include <dumux/discretization/elementsolution.hh>
#include <dumux/discretization/box.hh>
#include <dumux/discretization/cctpfa.hh>
#include <dumux/discretization/ccmpfa.hh>
#include <dumux/porousmediumflow/problem.hh>
#include <dumux/material/fluidmatrixinteractions/1p/thermalconductivityaverage.hh>
#include <dumux/material/components/cao2h2.hh>
#include <dumux/material/solidsystems/compositionalsolidphase.hh>
#include <dumux/material/fluidsystems/1padapter.hh>

#include "h2on2.hh"

// box solution dependent Neumann, outflow
#include <dumux/discretization/evalgradients.hh>
#include <dumux/discretization/evalsolution.hh>

#include "thermochemspatialparams.hh"
#include "thermochemreaction.hh"
#include <test/porousmediumflow/1pncmin/implicit/nonisothermal/modifiedcao.hh>
#include <dune/grid/yaspgrid.hh>

namespace Dumux
{

template <class TypeTag>
class ThermoChemChargeProblem;

namespace Properties{
// Create new type tags
namespace TTag {
struct ThermoChemCharge { using InheritsFrom = std::tuple<OnePNCMinNI>; };
struct ThermoChemChargeCCTpfa { using InheritsFrom = std::tuple<ThermoChemCharge, CCTpfaModel>; };
struct ThermoChemChargeCCMpfa { using InheritsFrom = std::tuple<ThermoChemCharge, CCMpfaModel>; };
struct ThermoChemChargeBox { using InheritsFrom = std::tuple<ThermoChemCharge, BoxModel>; };
}// end namespace TTag

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::ThermoChemCharge> { using type = ThermoChemChargeProblem<TypeTag>; };
// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::ThermoChemCharge> { using type = Dune::YaspGrid<1>; };
// The fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::ThermoChemCharge>
{
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using H2ON2 = FluidSystems::H2ON2<Scalar>;
    static constexpr auto phaseIdx = H2ON2::gasPhaseIdx; // simulate the air phase
    using type = FluidSystems::OnePAdapter<H2ON2, phaseIdx>;
};

template<class TypeTag>
struct SolidSystem<TypeTag, TTag::ThermoChemCharge>
{
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using ComponentOne = Components::ModifiedCaO<Scalar>;
    using ComponentTwo = Components::CaO2H2<Scalar>;
    using type = SolidSystems::CompositionalSolidPhase<Scalar, ComponentOne, ComponentTwo>;
};

// // Enable velocity output
// SET_BOOL_PROP(ThermoChemTypeTag, VtkAddVelocity, false);

// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::ThermoChemCharge>
{
    using FVGridGeometry = GetPropType<TypeTag, Properties::FVGridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = ThermoChemSpatialParams<FVGridGeometry, Scalar>;
};

// Define whether mole(true) or mass (false) fractions are used
template<class TypeTag>
struct UseMoles<TypeTag, TTag::ThermoChemCharge> { static constexpr bool value = true; };
}

/*!
 * \ingroup OnePNCMinTests
 *
 * \brief Test for the 1pncmin model in combination with the NI model for a quasi batch
 * reaction of Calciumoxyde to Calciumhydroxide.
 *
 * The boundary conditions of the batch test are such, that there are no gradients for temperature, pressure and gas water concentration within the reactor.
 *
 * To run the simulation execute the following line in shell:
 * <tt>./test_1pncminni_box -ParameterFile </tt>
 * The test only runs for the box discretization.
 */
template <class TypeTag>
class ThermoChemChargeProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using GridView = GetPropType<TypeTag, Properties::GridView>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using SolidSystem = GetPropType<TypeTag, Properties::SolidSystem>;
    using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using ElementVolumeVariables = typename GetPropType<TypeTag, Properties::GridVolumeVariables>::LocalView;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using Element = typename GridView::template Codim<0>::Entity;
    using FVElementGeometry = typename GetPropType<TypeTag, Properties::FVGridGeometry>::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using FVGridGeometry = GetPropType<TypeTag, Properties::FVGridGeometry>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using ReactionRate = ThermoChemReaction;
    using FluidState = GetPropType<TypeTag, Properties::FluidState>;
//     static const unsigned int phaseIdx = GET_PROP_VALUE(TypeTag, PhaseIdx);

    enum { dim = GridView::dimension };
    enum { dimWorld = GridView::dimensionworld };

    enum
    {
        // Indices of the primary variables
        pressureIdx = Indices::pressureIdx, //gas-phase pressure
        firstMoleFracIdx = FluidSystem::compIdx(FluidSystem::MultiPhaseFluidSystem::H2OIdx),
        CaOIdx = FluidSystem::numComponents,
        CaO2H2Idx = FluidSystem::numComponents+1,

        // Equation Indices
        conti0EqIdx = Indices::conti0EqIdx,

        // Phase Indices
        cPhaseIdx = SolidSystem::comp0Idx,
        hPhaseIdx = SolidSystem::comp0Idx+1,
        phaseIdx = FluidSystem::phase0Idx,

        temperatureIdx = Indices::temperatureIdx,
        energyEqIdx = Indices::energyEqIdx
    };

    using GlobalPosition = typename SubControlVolumeFace::GlobalPosition;

public:
    /*!
     * \brief The constructor
     *
     * \param fvGridGeometry The finite volume grid geometry
     */
    ThermoChemChargeProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry)
        : ParentType(fvGridGeometry)
    {
        std::cout<< "pressureIdx " <<  pressureIdx << "\n" <<
        "firstMoleFracIdx " <<  firstMoleFracIdx << "\n" <<
        "CaOIdx " <<  CaOIdx << "\n" <<
        "CaO2H2Idx " <<  CaO2H2Idx << "\n" <<
        "conti0EqIdx " <<  conti0EqIdx << "\n" <<
        "cPhaseIdx " <<  cPhaseIdx << "\n" <<
        "hPhaseIdx " <<  hPhaseIdx << "\n" <<
        "phaseIdx " <<  phaseIdx << "\n" <<
        "temperatureIdx " <<  temperatureIdx << "\n" <<
        "energyEqIdx " <<  energyEqIdx << "\n";

        name_      = getParam<std::string>("Problem.Name");
        FluidSystem::init(/*Tmin=*/573.15,
                          /*Tmax=*/773.15,
                          /*nT=*/50,
                          /*pmin=*/1e4,
                          /*pmax=*/9e6,
                          /*np=*/50);

        unsigned int codim = GET_PROP_TYPE(TypeTag, FVGridGeometry)::discMethod == DiscretizationMethod::box ? dim : 0;
        permeability_.resize(fvGridGeometry->gridView().size(codim));
        porosity_.resize(fvGridGeometry->gridView().size(codim));
        reactionRate_.resize(fvGridGeometry->gridView().size(codim));
        teq_.resize(fvGridGeometry->gridView().size(codim));

        // create and initialize file for flux and storage calculations
        outputFile_.open(name_+".out", std::ios::out);
        outputFile_ << "time | EnthaplyIn | EnthalpyOut | storageE | "  << std::endl;
    }

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const std::string name() const
    { return name_; }

    /*!
     * \brief Sets the currently used time step size.
     *
     * This is necessary to limit the source terms to the maximum possible rate.
     */
    void setTimeStepSize( Scalar timeStepSize )
     {
        timeStepSize_ = timeStepSize;
     }

    /*!
    * \brief Set the simulation time.
    *
    * \param t The current time.
    */
    void setTime(Scalar t)
    { time_ = t; }

    /*!
     * \name Boundary conditions
     *
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment
     *
     * \param globalPos The global position
     */
    BoundaryTypes boundaryTypesAtPos( const GlobalPosition &globalPos) const
    {
        BoundaryTypes values;

        values.setNeumann(pressureIdx);
        values.setNeumann(firstMoleFracIdx);
        values.setNeumann(temperatureIdx);
        values.setNeumann(CaO2H2Idx);
        values.setNeumann(CaOIdx);

        return values;
    }

    /*!
     * \brief Evaluates the boundary conditions for a Dirichlet
     *        boundary segment
     *
     * \param globalPos The global position
     */
    PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables priVars(0.0);

        if (globalPos[0] < eps_ )
        {
            priVars[pressureIdx] = 1.2e5;
            priVars[firstMoleFracIdx] = 0.01;
            priVars[temperatureIdx] = 773.15;
            priVars[CaO2H2Idx] = 0.2;
            priVars[CaOIdx] = 0.0;
        }

        if(globalPos[0] > this->fvGridGeometry().bBoxMax()[0] - eps_ )
        {
            priVars[pressureIdx] = 1e5;
            priVars[firstMoleFracIdx] = 0.01;
            priVars[temperatureIdx] = 773.15;
            priVars[CaO2H2Idx] = 0.2;
            priVars[CaOIdx] = 0.0;
        }

        return priVars;
    }

    /*!
     * \brief Evaluates the boundary conditions for a Neumann
     *        boundary segment in dependency on the current solution.
     *
     * \param element The element
     * \param fvGeometry The finite volume geometry
     * \param elemVolVars The element volume variables
     * \param scvf The subcontrolvolume face
     *
     * \f$ [ \textnormal{unit of conserved quantity} / (m^(dim-1) \cdot s )] \f$
     * Negative values indicate an inflow.
     */

   NumEqVector neumann(const Element& element,
                           const FVElementGeometry& fvGeometry,
                           const ElementVolumeVariables& elemVolVars,
                           const SubControlVolumeFace& scvf) const
    {
        NumEqVector flux(0.0);

        const auto& globalPos = scvf.ipGlobal();
        const auto& scv = fvGeometry.scv(scvf.insideScvIdx());

        if(globalPos[0] < eps_)
        {
           Scalar InFlowAir = 4.64;
           Scalar InFlowH2O = 0.01;
           Scalar tIn = 773.15;

           Scalar T = elemVolVars[scv].temperature();
           Scalar p = elemVolVars[scv].pressure(phaseIdx);

           FluidState fluidStateBorder;
           fluidStateBorder.setTemperature(tIn);
           fluidStateBorder.setPressure(phaseIdx, p);

           Scalar deltaH = 0.0; // if temperature at the right border > 573.15 K, cool down
                                //temperature of injected fluid via the enthalpyflux deltaH

           deltaH = -(T-tIn)* 1e5;

           Scalar hInAir = InFlowAir*FluidSystem::molarMass(firstMoleFracIdx-1)
                           *FluidSystem::componentEnthalpy(fluidStateBorder, phaseIdx, firstMoleFracIdx-1);

           Scalar hInH2O = InFlowH2O*FluidSystem::molarMass(firstMoleFracIdx)
                           *FluidSystem::componentEnthalpy(fluidStateBorder, phaseIdx, firstMoleFracIdx);
           flux[pressureIdx] = - InFlowAir; //[mol/s] gas inflow of the air component
           flux[firstMoleFracIdx] = - InFlowH2O;//[mol/s] gas inflow of the water component
           flux[temperatureIdx] = - (hInAir + hInH2O -deltaH); //[J/s] enthalpy inflow
        }
        // outflow BC
        if(globalPos[0] > this->fvGridGeometry().bBoxMax()[0] - eps_ )
        {
                        // set a fixed pressure on the right side of the domain
            const auto& ipGlobal = scvf.ipGlobal();
            const Scalar dirichletPressure = 1.0e5;
            const Scalar K = elemVolVars[scv].permeability();
            const Scalar molarDensity = elemVolVars[scv].molarDensity(phaseIdx);
            const Scalar density = elemVolVars[scv].density(phaseIdx);

            // construct the element solution
            const auto elemSol = [&]()
            {
                auto sol = elementSolution(element, elemVolVars, fvGeometry);
                return sol;
            }();
            // evaluate the gradient
            const auto gradient = [&]()->GlobalPosition
            {
                const auto& scvCenter = fvGeometry.scv(scvf.insideScvIdx()).center();;
                const Scalar scvCenterPresureSol = elemSol[0][pressureIdx];
                auto grad = ipGlobal- scvCenter;
                grad /= grad.two_norm2();  //see dune FieldVector; quadrierte Normierung
                grad *= (dirichletPressure - scvCenterPresureSol);
                return grad;
            }();
            // calculate the flux
            Scalar tpfaFlux = gradient * scvf.unitOuterNormal();
            tpfaFlux *= -1.0  * K;
// 
            if(tpfaFlux < 0) tpfaFlux = 0.0; //make sure that there is no influx from the right

            Scalar tpfaFluxMole = tpfaFlux * molarDensity * elemVolVars[scv].mobility(phaseIdx);
            Scalar tpfaFluxMass = tpfaFlux * density * elemVolVars[scv].mobility(phaseIdx);

            // emulate an outflow condition for the component transport on the right side
            flux[pressureIdx] = tpfaFluxMole * (elemVolVars[scv].moleFraction(phaseIdx, (firstMoleFracIdx-1)));
            flux[firstMoleFracIdx] = tpfaFluxMole * elemVolVars[scv].moleFraction(phaseIdx, firstMoleFracIdx);
            flux[temperatureIdx] = tpfaFluxMass * (FluidSystem::enthalpy(elemVolVars[scv].fluidState(), phaseIdx));

        }
       return flux;

    }

    /*!
     * \brief Evaluates the initial values for a control volume in
     *               \f$ [ \textnormal{unit of primary variables} ] \f$
     *
     * \param globalPos The global position
     */
    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    {
        PrimaryVariables priVars(0.0);

        priVars[pressureIdx] = 1e5;
        priVars[firstMoleFracIdx]   = 0.01;
        priVars[temperatureIdx] = 773.15;
        priVars[CaOIdx] = 0.0;
        priVars[CaO2H2Idx]   = 0.401;

        return priVars;
    }

    /*!
     * \brief Evaluate the source term for all phases within a given
     *        sub-control-volume in units of \f$ [ \textnormal{unit of conserved quantity} / (m^3 \cdot s )] \f$.
     *
     * This is the method for the case where the source term is
     * potentially solution dependent and requires some quantities that
     * are specific to the fully-implicit method.
     *
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry
     * \param elemVolVars All volume variables for the element
     * \param scv The subcontrolvolume
     *
     * For this method, the \a values parameter stores the conserved quantity rate
     * generated or annihilate per volume unit. Positive values mean
     * that the conserved quantity is created, negative ones mean that it vanishes.
     * E.g. for the mass balance that would be a mass rate in \f$ [ kg / (m^3 \cdot s)] \f$.
     */
    PrimaryVariables source(const Element &element,
                            const FVElementGeometry& fvGeometry,
                            const ElementVolumeVariables& elemVolVars,
                            const SubControlVolume &scv) const
    {

        PrimaryVariables source(0.0);
        const auto& volVars = elemVolVars[scv];

        Scalar qMass = rrate_.thermoChemReactionSimple(volVars);
        Scalar qMole = qMass/FluidSystem::molarMass(firstMoleFracIdx)*(1-volVars.porosity());

        source[conti0EqIdx+CaO2H2Idx] = qMole;
        source[conti0EqIdx+CaOIdx] = - qMole;
        source[conti0EqIdx+firstMoleFracIdx] = - qMole;

        Scalar deltaH = 108.3e3; // J/mol
        Scalar phi = volVars.porosity();
        source[energyEqIdx] = qMole * (deltaH - (phi/(1-phi))*(volVars.pressure(phaseIdx)/volVars.molarDensity(phaseIdx))) ;

        return source;
    }


   /*!
     * \brief Return the permeability
     */
    const std::vector<Scalar>& getPerm()
    {
        return permeability_;
    }

   /*!
     * \brief Return the porosity
     */
    const std::vector<Scalar>& getPoro()
    {
        return porosity_;
    }

     /*!
     * \brief Return the reaction rate
     */
    const std::vector<Scalar>& getRRate()
    {
        return reactionRate_;
    }

     /*!
     * \brief Return the equilibrium temperature
     */
    const std::vector<Scalar>& getTeq()
    {
        return teq_;
    }

    /*!
     * \brief Adds additional VTK output data to the VTKWriter. Function is called by the output module on every write.
     */
    void updateVtkOutput(const SolutionVector& curSol)
    {
        for (const auto& element : elements(this->fvGridGeometry().gridView()))
        {
            const auto elemSol = elementSolution(element, curSol, this->fvGridGeometry());

            auto fvGeometry = localView(this->fvGridGeometry());
            fvGeometry.bindElement(element);

            for (auto&& scv : scvs(fvGeometry))
            {
                VolumeVariables volVars;
                volVars.update(elemSol, *this, element, scv);
                const auto dofIdxGlobal = scv.dofIndex();
                permeability_[dofIdxGlobal] = this->spatialParams().permeability(element, scv, elemSol);
                porosity_[dofIdxGlobal] = volVars.porosity();
                PrimaryVariables reactionRate;
                reactionRate_[dofIdxGlobal] = (1-volVars.porosity())*rrate_.thermoChemReactionSimple(volVars);
                teq_[dofIdxGlobal] = -12845/(log(volVars.pressure(0)*volVars.moleFraction(0, firstMoleFracIdx)*1.0e-5) - 16.508);
            }
        }
    }

    void postTimeStep(NumEqVector& neumannInFlux, NumEqVector& neumannOutFlux, const SolutionVector& curSol)
    {
//         Scalar inNeumannN2 = 0.0;
//         Scalar inNeumannH20 = 0.0;
        Scalar inNeumannEnthalpy = 0.0;
//         Scalar outNeumannN2 = 0.0;
//         Scalar outNeumannH2O = 0.0;
        Scalar outNeumannEnthaply = 0.0;
//         Scalar outMass = 0.0;

        PrimaryVariables storage(0.0);
        Scalar energySource = 0.0;
        Scalar volumeTerm = 0.0;

        // Calculate Fluxes
        // Loop over all elements
        for (const auto& element : elements(this->fvGridGeometry().gridView()))
        {
            const auto elemSol = elementSolution(element, curSol, this->fvGridGeometry());
            auto fvGeometry = localView(this->fvGridGeometry());
            fvGeometry.bindElement(element);

//             auto eIdx = this->fvGridGeometry().elementMapper().index(element);

            for (auto&& scv : scvs(fvGeometry))
            {
                const auto volume = scv.volume();
                VolumeVariables volVars;
                volVars.update(elemSol, *this, element, scv);

                // calculate sum of source and correction term
                Scalar qMass = rrate_.thermoChemReactionSimple(volVars);
                Scalar qMole = qMass/FluidSystem::molarMass(firstMoleFracIdx)*(1-volVars.porosity());

                if (-qMole*timeStepSize_ + volVars.solidVolumeFraction(cPhaseIdx)* volVars.solidComponentMolarDensity(cPhaseIdx) < 0 + eps_)
                {
                    qMole = -volVars.solidVolumeFraction(cPhaseIdx)* volVars.solidComponentMolarDensity(cPhaseIdx)/timeStepSize_;
                }

                Scalar deltaH = 108.3e3; // J/mol
                energySource += /*scv.volume()**/ qMole * deltaH;
                volumeTerm += /*scv.volume()**/ (- qMole * (volVars.porosity()/(1-volVars.porosity()))*(volVars.pressure(phaseIdx)/volVars.molarDensity(phaseIdx)));

                // loop over all subcontrolvolumefaces
                for (auto&& scvf : scvfs(fvGeometry))
                {
//                     const auto idx = scvf.index();
                    const auto& ipGlobal = scvf.ipGlobal();

                    if(ipGlobal[0] < 0.0 + eps_)
                    {
//                         inNeumannN2 = neumannInFlux[firstMoleFracIdx-1];
//                         inNeumannH20 = neumannInFlux[firstMoleFracIdx];
                        inNeumannEnthalpy = neumannInFlux[temperatureIdx];
                    }

                    if(ipGlobal[0] > 0.08 - eps_)
                    {
//                         outNeumannN2 = neumannOutFlux[firstMoleFracIdx-1];
//                         outNeumannH2O = neumannOutFlux[firstMoleFracIdx];
                        outNeumannEnthaply = neumannOutFlux[temperatureIdx];
//                         outMass = (outNeumannN2 + outNeumannH2O)/ volVars.molarDensity() * volVars.density();
                    }


                }

                // calculate global storage
                storage[firstMoleFracIdx-1] += volVars.molarDensity(phaseIdx)*volVars.porosity()
                                              *volVars.moleFraction(phaseIdx, (firstMoleFracIdx-1))
                                              *volume;
                storage[firstMoleFracIdx] += volVars.molarDensity(phaseIdx)*volVars.porosity()
                                            *volVars.moleFraction(phaseIdx, firstMoleFracIdx)
                                            *volume;

                storage[CaOIdx] += volVars.solidVolumeFraction(cPhaseIdx)
                                    * volVars.solidComponentMolarDensity(cPhaseIdx)*volume;

                storage[CaO2H2Idx] += volVars.solidVolumeFraction(hPhaseIdx)
                                    * volVars.solidComponentMolarDensity(hPhaseIdx)*volume;

                storage[energyEqIdx] += (volVars.porosity()
                                * volVars.density(phaseIdx)
                                * volVars.internalEnergy(phaseIdx)
                                +
                                volVars.temperature()
                                * volVars.solidHeatCapacity()
                                * volVars.solidDensity()
                                * (1.0 - volVars.porosity()))
                                *volume;
            }

        }

        outputFile_ << time_<<" | "<< inNeumannEnthalpy <<" | " << outNeumannEnthaply <<" | " <<  storage[energyEqIdx]  << " | "  <<  std::endl;
    }

private:
    std::string name_;

    static constexpr Scalar eps_ = 1e-6;

    // boundary conditions
    Scalar boundaryPressure_;
    Scalar boundaryVaporMoleFrac_;
    Scalar boundaryTemperature_;

    std::vector<double> permeability_;
    std::vector<double> porosity_;
    std::vector<double> reactionRate_;
    std::vector<double> teq_;

    std::ofstream outputFile_;

    ReactionRate rrate_;
    Scalar timeStepSize_;
    Scalar episodeLength_;
    Scalar time_;
};

} //end namespace

#endif
