SUMMARY
=======
This is the DuMuX module containing the code for producing the results
submitted for:

G. Seitz, R. Helmig and H. Class<br>
__A numerical modeling study on the influence of porosity changes during thermochemical heat storage__



Installation
============

The easiest way to install this module and its dependencies is to create a new
directory and clone this module:

```
mkdir New_Folder && cd New_folder
git clone https://git.iws.uni-stuttgart.de/dumux-pub/seitz2019a.git
```

After that, execute the file [installSeitz2019a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Seitz2019a/raw/master/installSeitz2019a.sh)

```
chmod u+x seitz2019a/installSeitz2019a.sh
./seitz2019a/installSeitz2019a.sh
```


This should automatically download all necessary modules and check out the correct versions.

Finally, run

```
./dune-common/bin/dunecontrol --opts=seitz2019a/cmake.opts all
```

Installation with Docker
========================

Create a new folder in your favourite location and change into it

```bash
mkdir New_Folder
cd New_Folder
```

Download the container startup script by running
```bash
wget https://git.iws.uni-stuttgart.de/dumux-pub/seitz2019a/-/raw/master/docker_seitz2019a.sh
```

Open the Docker Container
```bash
bash docker_seitz2019a.sh open
```

Applications
============


__Building from source__: 

In order run the simulations of the different cases navigate to the folder

```bash
cd seitz2019a/build-cmake/appl
```

Compile the programms:

```bash
make test_discharge_constporo
make test_discharge_changingporo
make test_charge_constantporo
make test_charge_changingporo
```

And choose the respective input files to run the programms:

* Charge:
  - Case 1 ```./test_charge_constantporo charge_constporo_constperm.input```
  - Case 2 ```./test_charge_changingporo charge_poro_constperm.input```
  - Case 3 ```./test_charge_changingporo charge_poro_Kozeny.input```
  - Case 4 ```./test_charge_changingporo charge_poro_power.input```

* Discharge:
  - Case 1 ```./test_discharge_constporo discharge_constporo_constperm.input```
  - Case 2 ```./test_discharge_changingporo discharge_poro_constperm.input```
  - Case 3 ```./test_discharge_changingporo discharge_poro_Kozeny.input```
  - Case 4 ```./test_discharge_changingporo discharge_poro_power.input```


